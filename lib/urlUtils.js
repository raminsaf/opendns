var URLModule = require("url");
var crypto = require('crypto');


exports.normalizeUrl = function(url) {
    var urlObj = URLModule.parse(url);
    var ret = URLModule.format(urlObj);
    return ret;
}

exports.getUrlHash = function(url) {
    var sha256 = crypto.createHash("sha256");
    var chance = require('chance').Chance();
    sha256.update(url, "utf8");
    return sha256.digest("base64");
}

