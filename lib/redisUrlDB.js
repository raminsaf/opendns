var redis = require("redis");
var urlUtils = require("../lib/urlUtils.js");

var redis_servers = [ // Can be N different servers
  {host:"localhost", port: 6379},
  {host:"localhost", port: 6379},
  {host:"localhost", port: 6379},
  {host:"localhost", port: 6379}
  ];
var redis_port = 6379;
var redisShards = new Array();
for (var idx = 0; idx < redis_servers.length; idx++) {
  redisShards[idx] = redis.createClient(redis_servers[idx].port, redis_servers[idx].host);
}

exports.putUrl = function(hashedUrl, threatLevel, req, res, next) {
    shard = hashedUrl.charCodeAt(0) % redisShards.length;
    var EVIL_URLS = redisShards[shard];
    //TODO: Handle errors
    EVIL_URLS.set(hashedUrl, threatLevel);
    
    next(req, res);
};

exports.getUrl = function(hashedUrl, req, res, next) {
    shard = hashedUrl.charCodeAt(0) % redisShards.length;
    var EVIL_URLS = redisShards[shard];
    
    EVIL_URLS.get(hashedUrl, function(err, threatValue) {
        if (err) {
            next(null, req, res, err);
        } else {
            next(threatValue, req, res, err);
        }
    });
};
