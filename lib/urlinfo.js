var urlUtils = require("./urlUtils");

var GOOD = 0;
var EVIL = 1;

exports.GOOD = GOOD;
exports.EVIL = EVIL;

exports.checkUrl = function(req, res, url, evilUrlDB) {
    var normalized = urlUtils.normalizeUrl(url);
    var hashedUrl = urlUtils.getUrlHash(normalized);
    evilUrlDB.getUrl(hashedUrl, req, res, 
        function(threatLevel, req, res, err) {
            if (err) { // Something is really wrong here 5xx
                //Probably want to mask the real error from API user"
                res.json(500, { "error": "Something bad happened" });
            } else if (threatLevel) {
                res.json(200, { "risk": EVIL, "threatLevel": threatLevel });
            } else {
                res.json(200, { "risk": GOOD, "threatLevel": 0 });
            }
    });
};

exports.addUrl = function(req, res, url, threatLevel, evilUrlDB) {
    //TODO: Check for arg sanity
    var normalized = urlUtils.normalizeUrl(url);
    var hashedUrl = urlUtils.getUrlHash(normalized);
     
    evilUrlDB.putUrl(hashedUrl, threatLevel, req, res, 
        function(req, res, err) {
            if (err) { // Something is really wrong here 5xx
                //Probably want to mask the real error from API user"
                res.json(500, { "error": "Something bad happened" });
            } else {
                res.json(201, { "url": normalized, "threatLevel": threatLevel });
            } 
    });
};

