var urlInfo = require("../lib/urlinfo");
var redisUrlDB = require("../lib/redisUrlDB");

exports.check = function(req, res) {
    // Len of "/urlinfo/1/" = 11
    var url = req.url.substring(11);
    urlInfo.checkUrl(req, res, url, redisUrlDB);
};

exports.add = function(req, res) {
    //TODO: Check for req body sanity, sent 408 if url or threatLevel is missing
    var url = req.body.url;
    var threatLevel = req.body.threatLevel;
    urlInfo.addUrl(req, res, url, threatLevel, redisUrlDB);
};
