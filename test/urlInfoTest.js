var expect = require("chai").expect;

var mockUrlDB = require("./mockUrlDB");
var urlInfo = require("../lib/urlinfo");
var urlUtils = require("../lib/urlUtils");

var getUrlInfoRoute = require("../routes/urlInfo");

function initUrlInfoDB(evilUrls) {
    for (var url in evilUrls) {
        var normalized = urlUtils.normalizeUrl(url);
        var hashed = urlUtils.getUrlHash(normalized);
        mockUrlDB.putUrl(hashed, evilUrls[url], function(){});
    }
}

describe("UrlInfoLib", function(){
    describe("#checkUrl()", function(){
        it("Sets url threats and verifies the threats using a mock URL DB", function(){
            initUrlInfoDB({ "www.evil.com":56, "www.mordor.com":33 });

            var goodUrls = ["www.angel.com", "www.science.com"];
            for (var idx = 0; idx < goodUrls.length; idx++) {
                var req = { };
                var res = { };
                res.json = function(code, body) { this.body = body; this.code = code; };
                urlInfo.checkUrl(req, res, goodUrls[idx], mockUrlDB);
                expect(res.body).to.have.a.property("risk", urlInfo.GOOD);
            }

            var evilUrls = ["www.evil.com", "www.mordor.com"];
            for (var idx = 0; idx < evilUrls.length; idx++) {
                var req = { };
                var res = { };
                res.json = function(code, body) { this.body = body; this.code = code; };
                urlInfo.checkUrl(req, res, evilUrls[idx], mockUrlDB);
                expect(res.body).to.have.a.property("risk", urlInfo.EVIL);
                expect(res.body).to.have.a.property("threatLevel", initUrlInfoDB[evilUrls[idx]]);
            }
        });
    });
});


describe("UrlInfoRoutes", function() {
    describe("#add", function() {
        it("Adds a new threat (will fail if redis UrlDB is not available)", function() {
            var req = { body: { url: "www.google.com", threatLevel: 1234 } };
            var res = { };
            res.json = function(code, body) { this.body = body; this.code = code; };
            getUrlInfoRoute.add(req, res);
            expect(res.body).to.have.a.property("url", "www.google.com");
            expect(res.body).to.have.a.property("threatLevel", 1234);
        });
    });

    describe("#check", function() {
        it("Checks a threat (will fail if redis UrlDB is not available)", function() {
            var req = { };
            var res = { };
            res.json = function(code, body) { this.body = body; this.code = code; };
            req.url = "/urlinfo/1/www.google.com";
            getUrlInfoRoute.check(req, res);
            //expect(res.body).to.have.a.property("risk", urlInfo.EVIL);
            //expect(res.body).to.have.a.property("threatLevel", 1234);
        });
    });
});
